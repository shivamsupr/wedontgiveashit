$(document).ready(function() {

	//FlatUI colors array
	var colors = ["1abc9c", "2ecc71", "3498db", "9b59b6", "34495e", "27ae60", "2980b9", "8e44ad", "16a085", "2c3e50", "f1c40f", "e67e22", "e74c3c", "95a5a6", "f39c12", "d35400", "c0392b", "bdc3c7", "7f8c8d"];
    var fontFamilies = ['Akura Popo',
    'Barrio-Regular',
    'Canter Bold 3D',
    'Canter Bold Shadow',
    'Canter Bold Strips',
    'Canter Bold',
    'Canter Light',
    'Canter Outline',
    'GearusBold',
    'GearusBook',
    'GearusHeavy',
    'GearusLight',
    'GearusSerifThin',
    'GearusThin',
    'KlinicSlabBold'];
	//Pressing the space bar to change Background color
	$(document).on('keyup', 'body', function(e) {
		var self = $(this);

		if(e.keyCode === 32) {
            var color = "#" + colors[Math.floor(Math.random() * colors.length)];
			var fontfamily = fontFamilies[Math.floor(Math.random() * fontFamilies.length)];
			var maxLeftPadding = $(window).width() - $('.title').width();
            var maxTopPadding = $(window).height() - $('.title').height();
            var currentLeftPadding = Math.random() * maxLeftPadding;
            var currentTopPadding = Math.random() * maxTopPadding;

            self.css({
                'padding-left': currentLeftPadding / 2,
                'padding-top': currentTopPadding / 1.5,
                'font-family': fontfamily,
                'background-color': color
            });
        }
	});

});